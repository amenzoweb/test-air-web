import { Controller, Get, Query, Req, UseGuards } from '@nestjs/common';
import { ProductsService } from './products.service';
import { Product } from './product.entity';
import { OptionalAuthGuard } from '../auth/AuthOptional.guard';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiQuery } from '@nestjs/swagger';
import { RequestCustom } from '../shared/definitions/request.definition';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {
  }

  // ---
  // GET
  // ---
  @ApiOperation({ summary: 'Get/Search for products', description: 'Public visible products if user is not logged in, products for authenticated if user is logged in' })
  @ApiOkResponse({ type: [Product] })
  @ApiQuery({
    name: 'searchTerm',
    type: String,
    example: 'abonnement',
    description: 'Product search by label or description',
    required: false
  })
  @ApiBearerAuth('jwt-auth')
  @Get('')
  @UseGuards(OptionalAuthGuard)
  async getForAuthenticate(@Query('searchTerm') searchTerm: string, @Req() req: RequestCustom): Promise<Product[]> {
    return await this.productsService.get(!!req.user, searchTerm);
  }
}
