import * as dateFNS from 'date-fns';
import * as winston from 'winston';
// @ts-ignore
import { LoggerInstance, LoggerOptions } from 'winston';
import { join } from 'path';
import * as PrettyError from 'pretty-error';
import * as chalk from 'chalk';

export class LoggerService {
  private readonly logger: LoggerInstance;
  private readonly prettyError = new PrettyError();

  public static loggerOptions: LoggerOptions = {
    level: 'debug',
    format: winston.format.json(),
    transports: [
      new winston.transports.File({
        filename: join(__dirname, '..', '..', '..', '_data', 'logs', 'logs-' + dateFNS.format(new Date(), 'yyyyMMdd') + '.log'),
        // @ts-ignore
        json: true,
        prettyPrint: true,
        timestamp: true
      })
    ]
  };

  constructor(private context: string) {
    this.logger = (winston as any).createLogger(LoggerService.loggerOptions);
    this.prettyError.skipNodeFiles();
    this.prettyError.skipPackage('express', '@nestjs/common', '@nestjs/core');
  }


  log(data: string | { message: string; status: number }): void {
    const message = typeof data === 'object' && data.message ? data.message : data;
    const status = typeof data === 'object' && data.status ? data.status : null;

    this.logger.info(message, {
      timestamp: new Date().toISOString(),
      context: this.context,
      status
    });
    this.formattedLog('info', message);
  }

  error(data: string | { message: string; status: number }, trace?: string): void {
    let message = typeof data === 'object' && data.message ? data.message : data;
    const status = typeof data === 'object' && data.status ? data.status : null;

    if (trace) {
      message += ` -> ${trace}`;
    }

    console.error(data);

    this.logger.error(message, {
      timestamp: new Date().toISOString(),
      context: this.context,
      status
    });
    this.formattedLog('error', message, status);
  }

  warn(data: string | { message: string; status: number }): void {
    const message = typeof data === 'object' && data.message ? data.message : data;
    const status = typeof data === 'object' && data.status ? data.status : null;

    this.logger.error(message, {
      timestamp: new Date().toISOString(),
      context: this.context,
      status
    });
    this.formattedLog('warning', message);
  }

  private formattedLog(level: string, message: string | { message: string; status: number }, status?: number): void {
    const color = chalk.default;
    const time = dateFNS.format(new Date(), 'HH:mm');
    let levelColor;

    switch (level) {
      case 'error':
        levelColor = 'red';
        break;
      case 'warning':
        levelColor = 'yellow';
        break;
      default:
        levelColor = 'blue';
        break;
    }

    console.log(
      `[${color[levelColor](level.toUpperCase())}] ` +
      (status ? `[${color[levelColor](status)}] ` : '') +
      `${color.dim.yellow.bold.underline(time)} ` +
      `[${color.green(this.context)}] ` +
      message
    );
  }
}
