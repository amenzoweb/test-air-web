import { UserDto } from '../../users/dto/user.dto';

export interface HeadersCustom extends Headers {
  language: string;
}

export interface RequestCustom extends Request {
  user: UserDto;
  headers: HeadersCustom;
}
