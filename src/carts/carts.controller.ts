import { Controller, Delete, Get, Post, Req, UseGuards } from '@nestjs/common';
import { CartsService } from './carts.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { Cart } from './cart.entity';
import { IdDto } from './id.dto';

@Controller('carts')
export class CartsController {
  constructor(private readonly cartsService: CartsService) {
  }

  @Get('')
  @ApiOperation({ summary: 'Get user cart' })
  @ApiOkResponse({ type: Cart })
  @ApiBearerAuth('jwt-auth')
  @UseGuards(AuthGuard())
  async getByUser(@Req() req: any): Promise<any> {
    return await this.cartsService.getByUser(req.user);
  }

  @Post('')
  @ApiOperation({ summary: 'Add product to cart' })
  @ApiOkResponse({ type: Cart })
  @ApiBody({ type: IdDto })
  @ApiBearerAuth('jwt-auth')
  @UseGuards(AuthGuard())
  async addProduct(@Req() req: any): Promise<any> {
    return await this.cartsService.addProduct(req.user, req.body.id);
  }

  @Delete('')
  @ApiOperation({ summary: 'Delete cardProduct from Cart' })
  @ApiBody({ type: IdDto })
  @UseGuards(AuthGuard())
  async removeProduct(@Req() req: any): Promise<any> {
    return await this.cartsService.removeProduct(req.user, req.body.id);
  }

}
