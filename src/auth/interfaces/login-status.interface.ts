import { UserDto } from '../../users/dto/user.dto';

export interface LoginStatus {
  user: UserDto;
  accessToken: any;
  expiresIn: any;
}
