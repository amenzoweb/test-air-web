import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { LoginUserDto } from './dto/user-login.dto';
import { User } from './entity/user.entity';
import { createHash } from 'crypto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepo: Repository<User>
  ) {
  }

  async findOne(options?: object): Promise<UserDto> {
    const user = await this.userRepo.findOne(options);
    return UsersService.toUserDto(user);
  }

  async findForAuthenticate({ email, password }: LoginUserDto): Promise<UserDto> {
    const user = await this.userRepo.findOne({ where: { email } });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
    }

    // compare passwords
    // Hashing (founded user id+ given password in the form) using md5 algorithm
    password = createHash('md5').update(`${user.id + password}`).digest('hex');

    if (password !== user.password_hash) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }

    return UsersService.toUserDto(user);
  }

  async findByPayload({ email }: any): Promise<UserDto> {
    return await this.findOne({ where: { email } });
  }

  private static toUserDto(data: User): UserDto {
    const { id, name, email } = data;
    return {
      id,
      name,
      email
    };
  };


}
