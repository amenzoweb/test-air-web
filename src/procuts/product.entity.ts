import { AfterLoad, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../categories/category.entity';

@Entity('products')
export class Product {

  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'Abonnement Annuel' })
  @Column({
    type: 'text',
    charset: 'utf8',
    nullable: false
  })
  label: string;

  @ApiProperty({ example: 'Titre illimité pendant 12 mois....' })
  @Column({
    type: 'text',
    charset: 'utf8',
    default: null
  })
  description: string;

  @ApiProperty({ example: '6000' })
  @Column({
    type: 'int',
    nullable: false
  })
  price: number;

  @ApiProperty({ example: 'https://picsum.photos/256/256' })
  @Column({
    type: 'text',
    charset: 'utf8',
    default: null
  })
  thumbnail_url: string;

  @ApiProperty({ example: false })
  @Column({
    type: 'tinyint',
    default: 0
  })
  visible_public: any;

  @ApiProperty({ example: true })
  @Column({
    type: 'tinyint',
    default: 1
  })
  visible_authenticated: any;

  @ApiProperty({ type: () => Category })
  @ManyToOne(() => Category, category => category.products)
  @JoinColumn({ name: 'category_id' })
  category?: Category;

  @AfterLoad()
  transform() {
    this.visible_public = (this.visible_public === 1);
    this.visible_authenticated = (this.visible_authenticated === 1);
  }
}
