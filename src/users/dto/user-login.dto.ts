import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginUserDto {
  @IsNotEmpty()
  @ApiProperty({ example: 'vincent@airweb.fr' })
  readonly email: string;

  @IsNotEmpty()
  @ApiProperty({ example: 'bonjour' })
  readonly password: string;
}
