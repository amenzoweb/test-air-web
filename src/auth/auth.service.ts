import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { LoginStatus } from './interfaces/login-status.interface';
import { LoginUserDto } from '../users/dto/user-login.dto';
import { JwtPayload } from './interfaces/payload.interface';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { UserDto } from '../users/dto/user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService
  ) {
  }


  async login(loginUserDto: LoginUserDto): Promise<LoginStatus> {
    // find user in db
    const user: UserDto = await this.usersService.findForAuthenticate(loginUserDto);

    // generate and sign token
    const token = this._createToken(user);

    return {
      user: user,
      email: user.email,
      ...token
    };
  }

  async validateUser(payload: JwtPayload): Promise<UserDto> {
    const user = await this.usersService.findByPayload(payload);
    if (!user) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    return user;
  }

  private _createToken({ name }: UserDto): any {
    const expiresIn = process.env.EXPIRESIN;

    const user: JwtPayload = { name };
    const accessToken = this.jwtService.sign(user);
    return {
      expiresIn,
      accessToken
    };
  }
}
