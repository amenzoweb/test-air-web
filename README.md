Airweb - Test Technique 
===
Bonjour !

Voici, comme demandé, le petit projet d'api que je viens de développer, en utilisant le framework : Nest

Ce projet est développé sous :

- Node v18.12.0
- npm 8.19.2
- Nest/Cli 9.1.5
- WebStorm

Pour lancer le projet :

1- Installer les packages : node_modules:

````npm i````

2- S'il manque le fichier d'environnement(.env), voici son contenu:

````
PORT=3000
DATABASE=DATABASE.sqlite
KEY=AIRWEBSECRETKEY
EXPIRATION=7d
````

3- Démarrer l'api
````npm run start````

Lien de l'api :
http://127.0.0.1:3000

Lien de la documentation de l'api avec Swagger:
http://127.0.0.1:3000/api






