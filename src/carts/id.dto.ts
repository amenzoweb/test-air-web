import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class IdDto {
  @ApiProperty({ example: 2 })
  @IsNotEmpty()
  id: number;
}
