import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from './shared/filters/http-exception.filters';
import { NextFunction, Request, Response } from 'express';
import * as bodyParser from 'body-parser';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { i18nMiddleware } from './shared/middleware/i18n.middleware';
import * as dotenv from 'dotenv';

// Environnement SET
dotenv.config();

async function bootstrap() {

  const logger = require('./shared/services/logger.service');

  const app = await NestFactory.create<NestExpressApplication>(require('./app.module').AppModule, {
    logger: new logger.LoggerService('app')
  });

  app.use(bodyParser.json({ limit: '10mb' }));
  app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

  app.enableCors();


  // Protect endpoints from receiving incorrect data
  app.useGlobalPipes(new ValidationPipe());
  // Add a global exceptions filter
  app.useGlobalFilters(new HttpExceptionFilter()); // IMPORTANT

  app.use(i18nMiddleware);

  app.use((request: Request, response: Response, next: NextFunction) => {
    response.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    next();
  });


  // --------------------------------
  // OPEN API (SWAGGER) DOCUMENTATION
  // --------------------------------
  const documentationConfig = new DocumentBuilder()
    .setTitle('API Documentation')
    .setVersion('1.0')
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT',
        description: 'Enter JWT token',
        in: 'header'
      },
      'jwt-auth'
    )
    .build();

  const document = SwaggerModule.createDocument(app, documentationConfig);
  SwaggerModule.setup('/api', app, document);

  await app.listen(process.env.PORT || 3000);
}

bootstrap()
  .then(() => {
    console.log(
      `\r\nURL\r\n----\r\nhttp://127.0.0.1:${process.env.PORT || 3000}`
    );

    console.log(
      `\r\nDocumentation API : \r\nURL\r\n----\r\nhttp://127.0.0.1:${process.env.PORT || 3000}/api`
    );
  })
  .catch(err => {
    console.error(err);
  });
