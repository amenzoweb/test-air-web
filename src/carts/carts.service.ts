import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Cart } from './cart.entity';
import { UserDto } from '../users/dto/user.dto';
import { Product } from '../procuts/product.entity';
import { ProductsService } from '../procuts/products.service';
import { CartProduct } from './cart_to_product.entity';

@Injectable()
export class CartsService {

  constructor(@InjectRepository(Cart) private readonly cartsRepository: Repository<Cart>,
              @InjectRepository(CartProduct) private readonly cartProductsRepository: Repository<CartProduct>,
              private productsService: ProductsService) {
  }

  // ---
  // GET
  // ---
  async getByUser(user: UserDto): Promise<Cart> {
    return await this.cartsRepository.findOne({
      relations: ['user', 'cartProducts', 'cartProducts.product', 'cartProducts.cart'],
      where: { user: { id: user.id } }
    });
  }

  // -----------------------
  // ADD PRODUCT TO CART
  // if no cart : create new
  // -----------------------
  async addProduct(user: UserDto, productId: number) {
    let userCart: Cart = await this.getByUser(user);
    const product: Product = await this.productsService.getById(productId);

    if (product) {
      if (!userCart) {
        // create new cart
        userCart = await this.cartsRepository.create({
          total: 0,
          quantity: 0,
          cartProducts: [],
          user: user
        });
        await this.cartsRepository.save(userCart);

        userCart = await this.getByUser(user);
      }


      let cartProduct = await this.cartProductsRepository.create({
        cart: userCart,
        product: product
      });

      await this.cartProductsRepository.save(cartProduct);

      // Update Quantity and total
      userCart = await this.getByUser(user);
      userCart.quantity = userCart.cartProducts.length;
      userCart.total = userCart.cartProducts.reduce(function(total, current: CartProduct) {
        return total + current.product.price;
      }, 0);
      await this.cartsRepository.save(userCart);

      return await this.getByUser(user);
    }

  }

  // -----------------------
  // ADD PRODUCT TO CART
  // if no cart : create new
  // -----------------------
  async removeProduct(user: UserDto, cartProductId: number) {

    await this.cartProductsRepository.delete({
      id: cartProductId
    });

    // Update Quantity and total
    let userCart: Cart = await this.getByUser(user);
    userCart.quantity = userCart.cartProducts.length;
    userCart.total = userCart.cartProducts.reduce(function(total, current: CartProduct) {
      return total + current.product.price;
    }, 0);
    await this.cartsRepository.save(userCart);

    return await this.getByUser(user);
  }

}
