import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { join } from 'path';
import { User } from './users/entity/user.entity';
import { CategoriesModule } from './categories/categories.module';
import { ProductsModule } from './procuts/products.module';
import { Category } from './categories/category.entity';
import { Product } from './procuts/product.entity';
import { CartsModule } from './carts/carts.module';
import { Cart } from './carts/cart.entity';
import { CartProduct } from './carts/cart_to_product.entity';
import { HeaderResolver, I18nJsonParser, I18nModule } from 'nestjs-i18n';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: join(__dirname,'..', '_data', 'db', process.env.DATABASE),
      entities: [User, Category, Product, Cart, CartProduct],
      synchronize: false,
      logging: true
    }),
    I18nModule.forRoot({
      parser: I18nJsonParser,
      parserOptions: {
        path: join(__dirname,'..', '_data', 'i18n')
      },
      // filePattern: '*.json',
      fallbackLanguage: 'en',
      // saveMissing: false,
      resolvers: [new HeaderResolver(['language'])]
    }),
    UsersModule,
    AuthModule,
    CategoriesModule,
    ProductsModule,
    CartsModule],
  controllers: [AppController],
  providers: []
})
export class AppModule {
}
