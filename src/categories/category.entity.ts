import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Product } from '../procuts/product.entity';

@Entity('categories')
export class Category {
  @PrimaryGeneratedColumn()
  id: number;


  @ApiProperty({ example: '6' })
  @Column({
    type: 'int',
    nullable: false
  })
  index: number;

  @ApiProperty({ example: 'Abonnement Annuel' })
  @Column({
    type: 'text',
    charset: 'utf8',
    nullable: false
  })
  label: string;

  @ApiProperty({ example: 'Titre illimité pendant une semaine....' })
  @Column({
    type: 'text',
    charset: 'utf8',
    default: null
  })
  description: string;

  @ApiProperty({ type: () => [Product] })
  @OneToMany(() => Product, product => product.category)
  products?: Product[];


}
