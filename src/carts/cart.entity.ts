import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../users/entity/user.entity';
import { CartProduct } from './cart_to_product.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity('carts')
export class Cart {

  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 6000 })
  @Column()
  total: number;

  @ApiProperty({ example: 2 })
  @Column()
  quantity: number;

  @ApiProperty({ type: [CartProduct] })
  @OneToMany(() => CartProduct, cartProduct => cartProduct.cart)
  cartProducts: CartProduct[];

  @ApiProperty({ type: [User] })
  @OneToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;
}
