import { Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Product } from '../procuts/product.entity';
import { Cart } from './cart.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity('cart_to_products')
export class CartProduct {

  @ApiProperty({ example: 1 })
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Cart, cart => cart.id)
  @JoinColumn({ name: 'cart_id' })
  cart: Cart;

  @ApiProperty({ type: () => Product })
  @OneToOne(() => Product)
  @JoinColumn({ name: 'product_id' })
  product: Product;
}
