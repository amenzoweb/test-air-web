import { Controller, Get } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { ApiOperation } from '@nestjs/swagger';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {
  }

  // ---
  // GET
  // ---
  @ApiOperation({ summary: 'Get product categories' })
  @Get('')
  async get(): Promise<any> {
    return await this.categoriesService.get();
  }

}
