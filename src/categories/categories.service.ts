import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Category } from './category.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CategoriesService {

  constructor(@InjectRepository(Category) private readonly CaegoriesRepository: Repository<Category>) {
  }

  // ---
  // GET
  // ---
  async get() {
    return await this.CaegoriesRepository.find();
  }

  // ----------
  // GET BY IDs
  // ----------
  async getByIds(ids: number[]) {
    return await this.CaegoriesRepository.findByIds(ids);
  }


}
