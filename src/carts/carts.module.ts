import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartsController } from './carts.controller';
import { CartsService } from './carts.service';
import { Cart } from './cart.entity';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { User } from '../users/entity/user.entity';
import { CartProduct } from './cart_to_product.entity';
import { ProductsModule } from '../procuts/products.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Cart,
      CartProduct,
      User
    ]),
    AuthModule,
    UsersModule,
    ProductsModule],
  controllers: [
    CartsController
  ],
  providers: [
    CartsService
  ],
  exports: [TypeOrmModule, CartsService]
})
export class CartsModule {
}
