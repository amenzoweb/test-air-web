import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Cart } from "../../carts/cart.entity";

@Entity("users")
export class User {

  @ApiProperty({ example: 3 })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: "Vincent" })
  @Column({
    type: "text",
    charset: "utf8",
    nullable: false
  })
  name: string;

  @ApiProperty({ example: "vincent@airweb.fr" })
  @Column({
    type: "text",
    charset: "utf8",
    nullable: false
  })
  email: string;

  @ApiProperty({ example: "password hashed" })
  @Column({
    type: "text",
    charset: "utf8",
    nullable: false
  })
  password_hash: string;

  cart?: Cart;

}
