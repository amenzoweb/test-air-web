import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const responseGlobal = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    const response: any = exception.getResponse();
    const code = typeof response === 'object' && response.code ? response.code : null;
    const message =
      typeof response === 'object' && response.message
        ? response.message
        : typeof response === 'string'
          ? response
          : null;

    responseGlobal.status(status).json({
      status,
      code,
      message,
      path: request.url,
      timestamp: new Date().toISOString()
    });
  }
}
