import { NextFunction } from 'express';
import { RequestCustom } from '../definitions/request.definition';

export function i18nMiddleware(req: RequestCustom, res: Response, next: NextFunction) {
  global.language = (typeof req.headers.language !== 'undefined' && req.headers.language) ? req.headers.language : 'en';
  next();
}
