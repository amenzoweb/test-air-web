import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Product } from './product.entity';
import { Like, Repository } from 'typeorm';
import { FindOptionsWhere } from 'typeorm/find-options/FindOptionsWhere';

@Injectable()
export class ProductsService {

  constructor(@InjectRepository(Product) private readonly productsRepository: Repository<Product>) {
  }

  // ----------
  // GET/SEARCH
  // ----------
  async get(isAuthenticated: boolean = false, searchTerm: String = null): Promise<Product[]> {
    const where: FindOptionsWhere<any>[] = [];

    if (searchTerm) {
      where.push({ visible_authenticated: isAuthenticated, visible_public: !isAuthenticated, label: Like(`%${searchTerm}%`) });
      where.push({ visible_authenticated: isAuthenticated, visible_public: !isAuthenticated, description: Like(`%${searchTerm}%`) });
    } else {
      where.push({ visible_authenticated: isAuthenticated, visible_public: !isAuthenticated });
    }

    return await this.productsRepository.find({
      where,
      relations: ['category']
    });
  }

  // ----------
  // GET BY IDs
  // ----------
  async getById(id: number) {
    return await this.productsRepository.findOne({ where: { id } });
  }

}
